import csv

import sys
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt




with open("Coffeebar_2013-2017.csv") as f:
    next(f)
    reader = csv.reader(f, delimiter=";")
    drinks = []
    food = []
    customers = []
    moments = []
    momentsDrinks = []
    momentsFood = []

    drinksTotalSold = []
    foodTotalSold = []
    for i, line in enumerate(reader):
        d, h = line[0].split(' ')
        if h not in moments:
            moments.append(h)
            momentsDrinks.append([])    #add new array of drinks to the list
            momentsFood.append([])      #add new array of foods to the list
        ind = moments.index(h)
        momentsDrinks[ind].append(line[2])  #adding the drink ordered at moment h
        momentsFood[ind].append(line[3])    #adding the food ordered at moment h

        if line[2] not in drinks:
            drinks.append(line[2])
            drinksTotalSold.append(1)
        else:
            index = drinks.index(line[2])
            drinksTotalSold[index] = drinksTotalSold[index] + 1

        if (line[3] not in food) and (line[3] != ''):
            food.append(line[3])
            foodTotalSold.append(1)
        elif (line[3] != ''):
            index = food.index(line[3])
            foodTotalSold[index] = foodTotalSold[index] + 1

        """ 
        #Appending the list of unique customers
        if line[1] not in customers:
            customers.append(line[1])
        """
    for x in range(0, len(momentsDrinks)):
        print("On average the probability of a customer at " + str(moments[x]) + " buying (drinks): ")
        f1 = open("prbDrinks.txt", "a+")
        probDrinks = []
        for z in range(0,len(drinks)):
            prob = (momentsDrinks[x].count(drinks[z]) / len(momentsDrinks[x]))
            probDrinks.append(prob)
            print("\t *"+str(drinks[z])+" is: \t"+str(prob))
        drinksString = ','.join(map(str, drinks))
        probDrinksString = ','.join(map(str, probDrinks))
        f1.write(str(moments[x]) + ";" +str(drinksString)+";"+str(probDrinksString)+"\n")
        f1.close()
        print("and the probability for buying (food):")
        f2 = open("prbFood.txt", "a+")
        probFood = []
        for z in range(0,len(food)):
            prob = (momentsFood[x].count(food[z]) / len(momentsFood[x]))
            probFood.append(prob)
            if food[z] is "":
                print("\t *nothing is: \t" + str(prob))
            else:
                print("\t *"+str(food[z])+" is: \t"+str(prob))
        foodString = ','.join(map(str, food))
        probFoodString = ','.join(map(str, probFood))
        f2.write(str(moments[x]) + ";" + str(foodString) + ";" + str(probFoodString) + "\n")
        f2.close()
    if '' in food: food.remove('')
    print("There are " + str(len(drinks)) + " different types of drinks: " + str(drinks))  # 6
    print("There are " + str(len(food)) + " different types of food: " + str(food))  # 4
    print("There were " + str(len(customers)) + " different customers during the 5-year period.") # 247989

    plt.bar(drinks, drinksTotalSold)
    plt.show()
    plt.bar(food, foodTotalSold)
    plt.show()
    print("The End")

