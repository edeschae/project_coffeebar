import csv
import matplotlib.pyplot as plt


with open("Coffeebar_2013-2017.csv") as f:
    next(f)
    reader = csv.reader(f, delimiter=";")
    returningCustomers = []
    oneTimeCustomers = []

    #variables for the plots
    totalReturningCustomersAtMoment = []
    totalOneTimeCustomersAtMoment = []
    moments = []


    for i, line in enumerate(reader):
        print(i)
        d, h = line[0].split(' ')

        if line[1] not in oneTimeCustomers:
            oneTimeCustomers.append(line[1])

        else:
            oneTimeCustomers.remove(line[1])
            if line[1] not in returningCustomers:
                returningCustomers.append(line[1])


        if h not in moments:
            moments.append(h)
            totalReturningCustomersAtMoment.append(0)
            totalOneTimeCustomersAtMoment.append(0)


            momentIndex = moments.index(h)
            if line[1] in returningCustomers and (momentIndex < len(totalReturningCustomersAtMoment)):
                totalReturningCustomersAtMoment[momentIndex] += 1
            else:
                totalOneTimeCustomersAtMoment[momentIndex] += 1
        else:
            momentIndex = moments.index(h)
            if line[1] in returningCustomers:
                totalReturningCustomersAtMoment[momentIndex] += 1
            else:
                totalOneTimeCustomersAtMoment[momentIndex] += 1

    print("Total returning customers: " + str(len(returningCustomers)) + "\nTotal one time Customers: " + str(len(oneTimeCustomers)))
    #returning customers: 1000
    #one time customers: 247493

    # Calculating the total amount of customers at any moment during the 5 years
    probabilityOneTimeCustomersAtMoment = []
    probabilityReturningCustomersAtMoment = []
    for i in range(0, len(moments)):
        total = (totalOneTimeCustomersAtMoment[i] + totalReturningCustomersAtMoment[i])
        probabilityOneTimeCustomersAtMoment.append(0)
        probabilityOneTimeCustomersAtMoment[i] = totalOneTimeCustomersAtMoment[i] /  total

        probabilityReturningCustomersAtMoment.append(0)
        probabilityReturningCustomersAtMoment[i] = totalReturningCustomersAtMoment[i] / total

    plt.title("Probability of having a returning or one time customer at any given time")
    plt.xlabel("Time [hh:mm:ss]")
    plt.ylabel("Probability")
    plt.grid(axis='y', linestyle='-')
    plt.plot(moments, probabilityReturningCustomersAtMoment, "r")
    plt.plot(moments, probabilityOneTimeCustomersAtMoment, "b")
    plt.show()

    print("The End :)")


