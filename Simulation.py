import Customers as cust
import csv
import random
#QUESTION POSEE SUR STACKOVERFLOW : https://stackoverflow.com/questions/50187944/picking-random-integer-not-in-a-list-between-range/50187975#50187975
bannedReturningCustomers = 0 #list of indexes of returning customers with no more budget
bannedReturningCustomersIndex = []

#Creating the returning customers
returningCustomers = []
for i in range(0,1000):
    if (i <= 333):
        newCustomer = cust.Customer("hipster") #Creating hipsters
        returningCustomers.append(newCustomer)
    else:
        newCustomer = cust.Customer("regReturn") #creating regular returning customers (called regReturn)
        returningCustomers.append(newCustomer)

#Going through every timestep of the file and simulating behaviour
with open("Coffeebar_2013-2017.csv") as f:
    next(f)
    reader = csv.reader(f, delimiter=";")

    df = open("simulations/simulation_prct10.csv", 'a+')
    df.write("TIME;CUSTOMER;DRINKS;FOOD\n")
    for i, line in enumerate(reader):
        CID = 0
        drinked = ""
        eaten = ""
        date, time = line[0].split(' ')

        prct = (int(date.split('-')[0]) - 2013) * 1.10

        # Simulation if price go up by 20 prct after 2015
        #if (int(date.split('-')[0]) >= 2015):
        #    prct = 0.2

        if(cust.choose(['returning','once'],[0.2,0.8]) == 'returning') and (bannedReturningCustomers<999):
            #returning customer
            #index = random.randrange(0,999)
            valid_indexes = list(set(range(0, 999)) - set(bannedReturningCustomersIndex))
            index = random.choice(valid_indexes)

            returningCustomers[index].buyFood(time, prct)
            returningCustomers[index].buyDrink(time, prct)
            drinked = returningCustomers[index].tellLastDrink()
            eaten = returningCustomers[index].tellLastFood()
            CID = returningCustomers[index].CID

            if  returningCustomers[index].isBanned() and (index not in bannedReturningCustomersIndex):
                bannedReturningCustomers = bannedReturningCustomers + 1
                bannedReturningCustomersIndex.append(index)
                print("Banning Returning Customer:" + str(index) + " Total:" + str(bannedReturningCustomers))

        else:
            #one-timer customer
            customerType = cust.choose(['tripadvisor','regOnce'],[0.1,0.9])
            oneTimeCustomer = cust.Customer(customerType)
            oneTimeCustomer.buyDrink(time, prct)
            oneTimeCustomer.buyFood(time, prct)
            drinked = oneTimeCustomer.tellLastDrink()
            eaten = oneTimeCustomer.tellLastFood()
            CID = oneTimeCustomer.CID
        if (str(eaten)=="None"):
            eaten = ""
        if (str(drinked)=="None"):
            drinked = ""
        df.write(line[0] + ";" + str(CID) + ";" + str(drinked) + ";" + str(eaten) + "\n")
        print(i)
    print("End of simulation")
    df.close()

#writing some buying history of 10 random returning customers to outputfile
print("Writing history file")
buyingHistoryFile = open("buying_history_prct10.txt","a+")
buyingHistoryFile.write("This file contains the buying history of 10 random returning customers\n")
i = 0
while (i<10):
    ind = random.randrange(0,999)
    buyingHistoryFile.write("\n("+str(i+1)+") CustomerID: "+ str(ind) +"\t history:\n \t"+ returningCustomers[ind].tellHistory())
    i = i+1
buyingHistoryFile.close()
print("History file written")